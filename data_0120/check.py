posts = open("all.txt",'r')

vocab = []
for line in posts.readlines():
    line = line.split()
    if len(line) == 2:
        line = str(line[0])
        if line not in vocab:
            #print(line)
            vocab.append((line))
vocab.sort()

out = open("vocab.txt",'w')
for w in vocab:
    out.write(str(w)+'\n')

posts.close()
out.close()
