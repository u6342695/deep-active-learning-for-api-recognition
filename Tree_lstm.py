from keras import backend as K, Model
from keras.engine.topology import Layer, Input
import numpy as np
import  tensorflow as tf
class Tree_lstm(Layer):

    def __init__(self, output_dim, **kwargs):
        self.output_dim = output_dim
        super(Tree_lstm, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        # self.kernel = self.add_weight(name='kernel',
        #                               shape=(input_shape[1], self.output_dim),
        #                               initializer='uniform',
        #                               trainable=True)
        self.wi = self.add_weight(name='wi',
                                      shape=(200, 200),
                                      initializer='uniform',
                                      trainable=True)
        self.wf = self.add_weight(name='wf',
                                      shape=(200, 200),
                                      initializer='uniform',
                                      trainable=True)
        self.wo = self.add_weight(name='wo',
                                      shape=(200, 200),
                                      initializer='uniform',
                                      trainable=True)
        self.wu = self.add_weight(name='wu',
                                      shape=(200, 200),
                                      initializer='uniform',
                                      trainable=True)
        self.ui = self.add_weight(name='ui',
                                      shape=(200, 200),
                                      initializer='uniform',
                                      trainable=True)
        self.uf = self.add_weight(name='uf',
                                      shape=(200, 200),
                                      initializer='uniform',
                                      trainable=True)
        self.uo = self.add_weight(name='uo',
                                      shape=(200, 200),
                                      initializer='uniform',
                                      trainable=True)
        self.uu = self.add_weight(name='uu',
                                      shape=(200, 200),
                                      initializer='uniform',
                                      trainable=True)
        super(Tree_lstm, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        # tx = tf.convert_to_tensor(x, dtype=tf.float32)
        # C=np.zeros((6,200))
        # C=tf.convert_to_tensor(x, dtype=tf.float32)
        # C=[]
        #
        # H=[]
        #
        # for i in range(6):
        I0=K.sigmoid(K.dot(x[:,0,:],self.wi))
        O0=K.sigmoid(K.dot(x[:,0,:],self.wo))
        U0=K.tanh(K.dot(x[:,0,:],self.wu))
        C0=I0*U0
        H0=O0*K.tanh(C0)

        I1=K.sigmoid(K.dot(x[:,1,:],self.wi))
        O1=K.sigmoid(K.dot(x[:,1,:],self.wo))
        U1=K.tanh(K.dot(x[:,1,:],self.wu))
        C1=I1*U1
        H1=O1*K.tanh(C1)

        I2=K.sigmoid(K.dot(x[:,2,:],self.wi))
        O2=K.sigmoid(K.dot(x[:,2,:],self.wo))
        U2=K.tanh(K.dot(x[:,2,:],self.wu))
        C2=I2*U2
        H2=O2*K.tanh(C2)

        jojo=x[:,3,:]
        I3=K.sigmoid(K.dot(jojo,self.wi))
        O3=K.sigmoid(K.dot(jojo,self.wo))
        U3=K.tanh(K.dot(jojo,self.wu))
        C3=I3*U3
        H3=O3*K.tanh(C3)

        jojo = x[:,4,:]
        I4 = K.sigmoid(K.dot(jojo, self.wi))
        O4 = K.sigmoid(K.dot(jojo, self.wo))
        U4 = K.tanh(K.dot(jojo, self.wu))
        C4 = I4 * U4
        H4 = O4 * K.tanh(C4)

        jojo = x[:,5,:]
        I5 = K.sigmoid(K.dot(jojo, self.wi))
        O5 = K.sigmoid(K.dot(jojo, self.wo))
        U5 = K.tanh(K.dot(jojo, self.wu))
        C5 = I5 * U5
        H5 = O5 * K.tanh(C5)

        jojo=x[:,6,:]
        h=H1+H2+H3+H4+H5+H0
        I=K.sigmoid(K.dot(jojo,self.wi)+K.dot(h,self.ui))
        F0=K.sigmoid(K.dot(jojo,self.wf)+K.dot(H0,self.uf))
        F1=K.sigmoid(K.dot(jojo,self.wf)+K.dot(H1,self.uf))
        F2=K.sigmoid(K.dot(jojo,self.wf)+K.dot(H2,self.uf))
        F3=K.sigmoid(K.dot(jojo,self.wf)+K.dot(H3,self.uf))
        F4=K.sigmoid(K.dot(jojo,self.wf)+K.dot(H4,self.uf))
        F5=K.sigmoid(K.dot(jojo,self.wf)+K.dot(H5,self.uf))

        # for i in range(6):
        #     F.append(K.sigmoid(K.dot(jojo,self.wf)+K.dot(H[i],self.wf)))
        O=K.sigmoid(K.dot(jojo,self.wo)+K.dot(h,self.uo))
        U = K.tanh(K.dot(jojo, self.wu)+K.dot(h,self.uu))
        M=I*U+F0*C0+F1*C1+F2*C2+F3*C3+F4*C4+F5*C5
        Output=O*K.tanh(M)
        return Output



        # return K.dot(x,self.wi)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.output_dim)

input = Input(batch_shape=(None,7,200))
tree=Tree_lstm(200)
output=tree(input)
model = Model(inputs=input, outputs=output)

jojo=np.random.rand(20,7,200)
dio=np.ones((20,200))
model.compile(loss='mse', optimizer='sgd')
for step in range(5):
    cost = model.train_on_batch(jojo, dio)
    print(cost)

